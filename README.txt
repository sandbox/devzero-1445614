
-- SUMMARY --

The Alike search module performs search of similar nodes, by 
comparing clear text of node fields + title, using php funtion "similar_text".

-- REQUIREMENTS --

None.

-- INSTALLATION --

Just install module as usual.
http://drupal.org/documentation/install/modules-themes

-- CONFIGURATION --

* Configure user permissions in Administration � People � Permissions �
   admin_menu module:

  - search alike nodes

    Users in roles with the "search alike nodes" permission can perform 
    search of similar nodes

* Parameters of search can be edited directly on search page
   Content � Searching alike nodes

-- CUSTOMIZATION --

* Node view to compare can be configured through special node display
  "Comparison view"
  
* For rendering of nodes, creating strings for comparison, module
  uses two hooks - "theme_alike_search_clear_node" and 
  "theme_alike_search_clear_field" so they can be overloaded in 
  theme in order to change input data for
  comparison.

-- TROUBLESHOOTING --

None.

-- FAQ --

Q: Where the similar node search page is?

A: Content � Searching alike nodes
   
   
Q: How to exclude some fields from comparing?

A: For this purpose module creates special node display "Comparison view"

-- CONTACT --

Current maintainers:
* Devzero - http://drupal.org/user/1298126
